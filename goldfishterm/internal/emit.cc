// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "goldfishterm/internal/emit.h"

#include <memory>
#include <ostream>

#include "third_party/abseil/absl/time/clock.h"
#include "third_party/abseil/absl/types/span.h"

namespace goldfishterm_internal {

void EmitBytes::Emit(std::ostream& out) const {
  out.write(bytes_.data(), bytes_.size());
}

void EmitDelay::Emit(std::ostream& out) const noexcept {
  out.flush();
  absl::SleepFor(delay_);
}

void Emit(absl::Span<const std::shared_ptr<const EmitTerm>> terms,
          std::ostream& out) {
  for (const auto& term : terms) {
    term->Emit(out);
  }
}

}  // namespace goldfishterm_internal
