// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include <getopt.h>
#include <locale.h>

#include <iostream>
#include <stdexcept>
#include <string>

#include "goldfishterm/terminfo.h"
#include "third_party/abseil/absl/strings/string_view.h"

constexpr absl::string_view kShortUsage =
    "Usage: find /usr/share/terminfo -type f -exec basename '{}' ';' | sort | "
    "uniq |\n               terminfo_system_test\n";

constexpr absl::string_view kHelp = R"(
Tests goldfishterm's terminfo parser against real-world binary terminfo files.
Reads terminal names, one per line, from standard input and attempts to look up
and parse the terminfo file associated with each one. Prints the terminal names
that failed lookup or parsing.

A Reverse Polish scientific calculator. Invoked directly, ec launches an
interactive interpreter that displays the stack and accepts input. Invoked on a
file, ec processes the entire file; if the result stack is nonempty, ec prints
the stack in space-separated form.

Options:
      --help                  display this help and exit
)";

constexpr absl::string_view kAskForHelp =
    "Try 'terminfo_system_test --help' for more information.\n";

enum {
  kHelpLongOption = 128,
};

int main(int argc, char* argv[]) {
  setlocale(LC_ALL, "");

  static option long_options[] = {
      {"help", no_argument, nullptr, kHelpLongOption},
      {nullptr, 0, nullptr, 0},
  };
  while (true) {
    int c = getopt_long(argc, argv, "", long_options, /*longindex=*/nullptr);
    if (c == -1) {
      break;
    }
    switch (c) {
      case kHelpLongOption:
        std::cout << kShortUsage << kHelp;
        return 0;
      case '?':
        std::cerr << kAskForHelp;
        return 1;
      default:
        std::cerr << "terminfo_system_test: internal error: unhandled getopt "
                  << "switch\nThis is a bug! Please report it.\n";
        return 1;
    }
  }

  if (optind != argc) {
    std::cerr << kShortUsage << kAskForHelp;
    return 1;
  }

  std::string line;
  int r = 0;
  while (std::getline(std::cin, line)) {
    try {
      goldfishterm::TerminfoEntry::FromSystemDatabase(line);
    } catch (const std::runtime_error&) {
      std::cout << line << '\n';
      r = 1;
    }
  }
  return r;
}
