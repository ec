// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "goldfishterm/simple.h"

#include <termios.h>

#include <stdexcept>
#include <vector>

#include "goldfishterm/internal/emit.h"
#include "goldfishterm/terminfo.h"
#include "third_party/abseil/absl/strings/str_cat.h"
#include "third_party/abseil/absl/strings/string_view.h"

namespace goldfishterm {

namespace {

using ::goldfishterm_internal::InterpretStringCapability;
using ::goldfishterm_internal::InterpretStringCapabilityInput;

StringCapability CursorVisibilityCapability(CursorVisibility v) {
  switch (v) {
    case CursorVisibility::kInvisible:
      return StringCapability::kCursorInvisible;
    case CursorVisibility::kNormal:
      return StringCapability::kCursorNormal;
    case CursorVisibility::kVeryVisible:
      return StringCapability::kCursorVisible;
    default:
      throw std::invalid_argument(absl::StrCat("invalid CursorVisibility ", v));
  }
}

}  // namespace

SimpleTerminalOutput::SimpleTerminalOutput(absl::string_view terminal_name,
                                           const termios& tty,
                                           std::ostream& out)
    : terminfo_(TerminfoEntry::FromSystemDatabase(terminal_name)),
      baud_(cfgetospeed(&tty)),
      out_(out) {}

void SimpleTerminalOutput::SetCursorVisibility(CursorVisibility v) {
  return Emit(CursorVisibilityCapability(v));
}

void SimpleTerminalOutput::Emit(
    StringCapability cap,
    std::vector<goldfishterm_internal::StringCapabilityParameter> parameters) {
  goldfishterm_internal::Emit(
      InterpretStringCapability(
          InterpretStringCapabilityInput(terminfo_, terminfo_.get(cap).value(),
                                         std::move(parameters), /*baud=*/baud_,
                                         /*lines_affected=*/1))
          .terms,
      out_);
}

}  // namespace goldfishterm
