EC, a terminal-based RPN calculator
===================================

EC (binary: `ec`) is a terminal-based RPN calculator inspired by
Hewlett-Packard’s venerable HP&nbsp;48 line. It aims to be the calculator of
choice for the discriminating computer scientist.

EC is ready for everyday use. It supports the four basic functions, powers and
roots (with `sq`/`sqrt` and `pow`/`xroot`), logarithms (`ln`/`exp` for base _e_,
`log`/`alog` for base 10), and trigonometry
(`sin`/`cos`/`tan`/`asin`/`acos`/`atan`). It is accessible via a batch mode (try
`echo 2 2 + | ec`), and if invoked interactively, it provides basic line
editing. On the other hand, arrow keys don’t work, the calculator is not
programmable, you can’t control the output format, there’s no undo support,
trigonometry is always in radians, there’s no support for complex numbers, and
the standard library is somewhat impoverished (e.g., `pick` is not yet
implemented). All of these will be fixed before we declare version 1.0; if you’d
like to help, please get in touch!

**EC’s input language is not yet stable.** At some point before version 1.0 is
declared, the input language will become stable, but for the time being, any
scripts you write in the EC language may be broken by future releases. If you’d
like to take on any serious scripting in EC, please get in touch; we may be able
to provisionally stabilize certain language features to avoid breaking you.

Currently, EC performs all computations with double-precision binary floats.
This means that all the ordinary issues with binary floating point come into
play; notably, many common decimal fractions (like 0.1) cannot be represented
exactly within EC. EC uses guard digits and conservative display settings to
prevent the most obvious roundoff errors (EC displays 0.1 as 0.1, not
0.10000000000000000555), and all told, EC’s precision is fairly close to that of
the original HP calculator line. Nonetheless, EC does not yet offer the same
level of precision as more recent calculators (e.g., the HP&nbsp;Prime) or true
computer mathematics systems (e.g.,
[Xcas](https://www-fourier.ujf-grenoble.fr/~parisse/giac.html)). If you’re doing
heavy-duty numerical analysis, EC is not yet the tool for you.

To build EC, you’ll need [our customized version of
Abseil](https://git.benjamin.barenblat.name/ec-abseil/), which is checked in as
a Git submodule. You’ll also need GCC, [Antlr&nbsp;4](https://www.antlr.org/),
[re2c](https://re2c.org/), and [Ninja](https://ninja-build.org/), none of which
is checked in; on a Debian system, you can run `apt install build-essential
ninja-build antlr4 libantlr4-runtime-dev re2c` to get the packages you need.
Fire up `ninja`, wait a bit, and you’ll soon have an `ec` binary in the
repository root. Enjoy!
