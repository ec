// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// Built-in functions.

#ifndef EC_SRC_BUILTIN_H_
#define EC_SRC_BUILTIN_H_

#include <memory>
#include <string>

#include "src/language.h"
#include "third_party/abseil/absl/container/flat_hash_map.h"

namespace ec {

// The initial environment.
absl::flat_hash_map<std::string, std::shared_ptr<const Term>>
BuiltinEnvironment() noexcept;

// Stack operations.
void BuiltinDup(State&);
void BuiltinDrop(State&);
void BuiltinSwap(State&);

// Basic unary operations.
void BuiltinNeg(State&);
void BuiltinInv(State&);
void BuiltinSq(State&);
void BuiltinSqrt(State&);
void BuiltinAlog(State&);
void BuiltinLog(State&);
void BuiltinExp(State&);
void BuiltinLn(State&);
void BuiltinSin(State&);
void BuiltinCos(State&);
void BuiltinTan(State&);
void BuiltinAsin(State&);
void BuiltinAcos(State&);
void BuiltinAtan(State&);
void BuiltinAbs(State&);

// Basic binary operations.
void BuiltinAdd(State&);
void BuiltinSub(State&);
void BuiltinMul(State&);
void BuiltinDiv(State&);
void BuiltinPow(State&);
void BuiltinXroot(State&);

}  // namespace ec

#endif  // EC_SRC_BUILTIN_H_
