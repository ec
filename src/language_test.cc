// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/language.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "src/language_matchers.h"
#include "third_party/abseil/absl/strings/str_cat.h"

namespace ec {
namespace {

using ::testing::ElementsAre;
using ::testing::IsEmpty;

void PushFive(State& s) { s.stack.push_back(GroundTerm::Make(5)); }

TEST(GroundTermTest, EvaluateMeansPushSelf) {
  State s;
  GroundTerm(42).Evaluate(s);
  EXPECT_THAT(s.stack, ElementsAre(PointsToGroundTerm(42)));
}

TEST(GroundTermTest, ShowsAsValue) { EXPECT_EQ(GroundTerm(42).Show(), "42"); }

TEST(ForeignProgramTermTest, EvaluateCallsFunction) {
  State s;
  ForeignProgramTerm(PushFive).Evaluate(s);
  EXPECT_THAT(s.stack, ElementsAre(PointsToGroundTerm(5)));
}

TEST(ForeignProgramTermTest, ShowsAsOpaque) {
  EXPECT_EQ(ForeignProgramTerm(PushFive).Show(), "<program>");
}

TEST(SymbolTermTest, BadLookup) {
  State s;
  SymbolTerm t("push5");
  EXPECT_THROW(t.Evaluate(s), UndefinedName);
}

TEST(SymbolTermTest, GoodLookup) {
  State s;
  s.environment["push5"] = ForeignProgramTerm::Make(PushFive);
  SymbolTerm("push5").Evaluate(s);
  EXPECT_THAT(s.stack, ElementsAre(PointsToGroundTerm(5)));
}

TEST(EvaluateAllTest, EvaluatesNothing) {
  State s;
  EvaluateAll({}, s);
  EXPECT_THAT(s.stack, IsEmpty());
}

TEST(EvaluateAllTest, EvaluatesMultiple) {
  State s;
  EvaluateAll({GroundTerm::Make(42), ForeignProgramTerm::Make(PushFive)}, s);
  EXPECT_THAT(s.stack,
              ElementsAre(PointsToGroundTerm(42), PointsToGroundTerm(5)));
}

}  // namespace
}  // namespace ec
