// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// Stream-oriented interfaces to EC.

#ifndef EC_SRC_UI_STREAM_H_
#define EC_SRC_UI_STREAM_H_

#include <iostream>
#include <istream>
#include <ostream>

#include "src/ui.h"

namespace ec {

// A line-mode interpreter working on streams.
class LineUi : public Ui {
 public:
  explicit LineUi(std::istream& in = std::cin, std::ostream& out = std::cout)
      : in_(in), out_(out) {}

  int Main() noexcept override;

 private:
  std::istream& in_;
  std::ostream& out_;
};

// A batch-mode interpreter working on streams.
class StreamUi : public Ui {
 public:
  explicit StreamUi(std::istream& in = std::cin, std::ostream& out = std::cout)
      : in_(in), out_(out) {}

  int Main() noexcept override;

 private:
  std::istream& in_;
  std::ostream& out_;
};

}  // namespace ec

#endif  // EC_SRC_UI_STREAM_H_
