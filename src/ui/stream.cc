// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/ui/stream.h"

#include <assert.h>

#include <istream>
#include <memory>
#include <ostream>
#include <string>

#include "src/error.h"
#include "src/language.h"
#include "src/parser_driver.h"
#include "third_party/abseil/absl/strings/str_join.h"

namespace ec {

namespace {

std::string ReadAll(std::istream& in) noexcept {
  std::string result;
  int bytes_read = 0;
  while (in.good()) {
    result.append(4096, '\0');
    in.read(result.data() + bytes_read, 4096);
    bytes_read += in.gcount();
  }
  assert(bytes_read <= result.size());
  result.resize(bytes_read);
  return result;
}

void ShowState(const State& state, std::ostream& out) noexcept {
  if (state.stack.empty()) {
    return;
  }
  out << absl::StrJoin(state.stack, " ", FormatStackElement) << '\n';
}

}  // namespace

int LineUi::Main() noexcept {
  State state;
  while (in_.good()) {
    std::string line;
    std::getline(in_, line);
    try {
      EvaluateAll(ParseFromString(line), state);
    } catch (const Error& e) {
      std::cerr << "ec: " << e.what() << '\n';
      return 1;
    }
  }
  ShowState(state, out_);
  return 0;
}

int StreamUi::Main() noexcept {
  State state;
  std::string program = ReadAll(in_);
  try {
    EvaluateAll(ParseFromString(program), state);
  } catch (const Error& e) {
    std::cerr << "ec: " << e.what() << '\n';
    return 1;
  }
  ShowState(state, out_);
  return 0;
}

}  // namespace ec
