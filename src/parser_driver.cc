// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/parser_driver.h"

#include <antlr4-runtime.h>

#include <memory>
#include <vector>

#include "src/CalculatorBaseVisitor.h"
#include "src/CalculatorLexer.h"
#include "src/CalculatorParser.h"
#include "src/language.h"
#include "third_party/abseil/absl/strings/string_view.h"

namespace ec {

namespace {

template <typename T, typename... Args>
std::shared_ptr<const Term> MakeTerm(Args... args) {
  return std::static_pointer_cast<const Term>(std::make_shared<T>(args...));
}

class Visitor : public CalculatorBaseVisitor {
 public:
  antlrcpp::Any visitProgram(CalculatorParser::ProgramContext* ctx) override {
    Program p;
    for (CalculatorParser::TermContext* term : ctx->term()) {
      p.push_back(visit(term));
    }
    return p;
  }

  antlrcpp::Any visitNumber(CalculatorParser::NumberContext* ctx) override {
    return MakeTerm<GroundTerm>(ctx->value);
  }

  antlrcpp::Any visitIdentifier(
      CalculatorParser::IdentifierContext* ctx) override {
    return MakeTerm<SymbolTerm>(ctx->value);
  }

  antlrcpp::Any visitError(CalculatorParser::ErrorContext*) override {
    throw ParseError();
  }
};

}  // namespace

Program ParseFromString(absl::string_view text) {
  antlr4::ANTLRInputStream input(text.data(), text.size());
  CalculatorLexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);
  CalculatorParser parser(&tokens);
  return Visitor().visit(parser.program());
}

}  // namespace ec
