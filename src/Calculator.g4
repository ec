// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// The lexer and parser grammars for the EC language.

grammar Calculator;

options {
  language = Cpp;
}

@parser::postinclude {
#include <stdexcept>
#include <string>
#include <utility>

#include "third_party/abseil/absl/strings/numbers.h"
}

program : term* ;

term : number | identifier | error ;

number returns [double value]
  : s=NUMBER {
      if (!absl::SimpleAtod($s->getText(), &$value)) {
        throw std::runtime_error("Calculator: parser produced an invalid double");
      }
    }
  ;

identifier returns [std::string value]
  : s=IDENTIFIER {
      $value = std::move($s->getText());
    }
  | SUGARED_ADD { $value = "add"; }
  | SUGARED_SUB { $value = "sub"; }
  | SUGARED_MUL { $value = "mul"; }
  | SUGARED_DIV { $value = "div"; }
  ;

error : ERROR ;

NUMBER : [+-]? (DIGIT+ '.'? DIGIT* | '.' DIGIT+) ('e' [+-]? DIGIT+)? ;

IDENTIFIER : [\p{Alpha}\p{General_Category=Other_Letter}] [\p{Alnum}\p{General_Category=Other_Letter}]* ;

SUGARED_ADD : '+' ;
SUGARED_SUB : '-' ;
SUGARED_MUL : '*' ;
SUGARED_DIV : '/' ;

WS : [\p{White_Space}]+ -> skip ;

ERROR : . ;

fragment DIGIT : [0-9] ;
