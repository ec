// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// gMock matchers for Terms.

#ifndef EC_SRC_LANGUAGE_MATCHERS_H_
#define EC_SRC_LANGUAGE_MATCHERS_H_

#include <gmock/gmock.h>

#include "src/language.h"
#include "third_party/abseil/absl/strings/str_cat.h"

namespace ec {

MATCHER_P(PointsToGroundTerm, d,
          absl::StrCat("points to GroundTerm(", d, ")")) {
  using ::testing::DoubleEq;
  using ::testing::Pointee;
  using ::testing::Property;
  using ::testing::WhenDynamicCastTo;

  return ExplainMatchResult(Pointee(WhenDynamicCastTo<const GroundTerm&>(
                                Property(&GroundTerm::value, DoubleEq(d)))),
                            arg, result_listener);
}

MATCHER_P(PointsToSymbolTerm, s,
          absl::StrCat("points to SymbolTerm(", s, ")")) {
  using ::testing::Eq;
  using ::testing::Pointee;
  using ::testing::WhenDynamicCastTo;

  return ExplainMatchResult(
      Pointee(WhenDynamicCastTo<const SymbolTerm&>(Eq(SymbolTerm(s)))), arg,
      result_listener);
}

}  // namespace ec

#endif  // EC_SRC_LANGUAGE_MATCHERS_H_
