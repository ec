// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/util.h"

#include <cxxabi.h>
#include <stdlib.h>

#include <string>

std::string DemangleIfPossible(const char* name) noexcept {
  int status;
  char* demangled_raw = abi::__cxa_demangle(name, /*output_buffer=*/nullptr,
                                            /*length=*/0, &status);
  std::string demangled;
  if (demangled_raw != nullptr) {
    demangled = std::string(demangled_raw);
  }
  free(demangled_raw);
  return demangled;
}
