// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

// A driver for the Antlr-generated EC parser.

#ifndef EC_SRC_PARSER_DRIVER_H_
#define EC_SRC_PARSER_DRIVER_H_

#include <memory>
#include <vector>

#include "src/error.h"
#include "src/language.h"
#include "third_party/abseil/absl/strings/string_view.h"

namespace ec {

class ParseError : public Error {
 public:
  explicit ParseError() : Error("parse error") {}
};

// Converts a bare string to an EC program.
Program ParseFromString(absl::string_view);

}  // namespace ec

#endif  // EC_SRC_PARSER_DRIVER_H_
