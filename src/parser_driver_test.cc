// Copyright 2021 Benjamin Barenblat
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

#include "src/parser_driver.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "src/language.h"
#include "src/language_matchers.h"

namespace ec {
namespace {

using ::testing::ElementsAre;
using ::testing::IsEmpty;

TEST(ParserDriverTest, LexerError) {
  EXPECT_THROW(ParseFromString("!!!"), ParseError);
}

TEST(ParserDriverTest, EmptyProgram) {
  EXPECT_THAT(ParseFromString(""), IsEmpty());
}

TEST(ParserDriverTest, ConvertsNumber) {
  EXPECT_THAT(ParseFromString("12321"), ElementsAre(PointsToGroundTerm(12321)));
}

TEST(ParserDriverTest, ConvertsIdentifier) {
  EXPECT_THAT(ParseFromString("dup"), ElementsAre(PointsToSymbolTerm("dup")));
}

TEST(ParserDriverTest, ParsesMultipleTerms) {
  EXPECT_THAT(ParseFromString("2 2 + 4 /"),
              ElementsAre(PointsToGroundTerm(2), PointsToGroundTerm(2),
                          PointsToSymbolTerm("add"), PointsToGroundTerm(4),
                          PointsToSymbolTerm("div")));
}

}  // namespace
}  // namespace ec
